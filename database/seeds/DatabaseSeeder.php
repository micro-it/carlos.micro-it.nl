<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Coffins
        \App\Coffin::create([
            'name' => 'Kistje 1',
            'price' => 100,
        ]);
        \App\Coffin::create([
            'name' => 'Kistje 2',
            'price' => 120,
        ]);

        // Cards
        \App\Card::create([
            'name' => 'Kaartje 1',
            'price' => 10,
        ]);
        \App\Card::create([
            'name' => 'Kaartje 2',
            'price' => 12,
        ]);

        // Order
        $order = \App\Order::create([
            'client_name' => 'Jeroen',

        ]);

        // Get order items
        $order_coffin = \App\Coffin::whereName('Kistje 1')->firstOrFail();
        $order_card = \App\Card::whereName('Kaartje 1')->firstOrFail();

        // Insert
        $order->lines()->create([
            'quantity' => 1,
            'price' => $order_coffin->price,
            'item_type' => \App\Coffin::class,
            'item_id' => $order_coffin->id,
        ]);

        $order->lines()->create([
            'quantity' => 1,
            'price' => $order_card->price,
            'item_type' => \App\Card::class,
            'item_id' => $order_card->id,
        ]);
    }
}
