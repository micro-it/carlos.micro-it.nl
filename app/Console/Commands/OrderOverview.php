<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;

class OrderOverview extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'carlos:order-overview';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Overview of all orders';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::orderBy('created_at')->get();

        $headers = ['Order ID', 'Client name', '# items', 'Total price'];
        $data = [];

        foreach ($orders as $order) {
            $data[] = [
                $order->id,
                $order->client_name,
                count($order->lines),
                $order->total_price,
            ];
        }

        $this->table($headers, $data);
    }
}
