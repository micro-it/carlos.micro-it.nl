<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $visible = [
        'id',
        'client_name',
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'client_name',
        'medium_id',
        'store_id',
    ];

    protected $queryable = [
        'id',
        'client_name',
    ];

    protected $appends = [
        //
    ];

    public function lines()
    {
        return $this->hasMany(OrderLine::class);
    }

    public function getTotalPriceAttribute()
    {
        $price = 0;

        foreach ($this->lines as $line) {
            $price += $line->total_price;
        }

        return $price;
    }
}
