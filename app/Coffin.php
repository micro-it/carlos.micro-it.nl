<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coffin extends Model
{

    protected $visible = [
        'id',
        'name',
        'price',
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'client_name',
        'medium_id',
        'store_id',
    ];

    protected $queryable = [
        'id',
        'client_name',
    ];

    protected $casts = [
        'price' => 'float',
    ];

    protected $appends = [
        //
    ];
}
