<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderLine extends Model
{

    protected $visible = [
        'id',
        'order_id',
        'quantity',
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'order_id',
        'quantity',
    ];

    protected $queryable = [
        'id',
        'order_id',
        'quantity',
    ];

    protected $appends = [
        'total_price',
    ];

    public function item()
    {
        return $this->morphTo();
    }

    public function getTotalPriceAttribute()
    {
        return $this->price * $this->quantity;
    }
}
