module.exports = function (gulp, plugins, paths) {
    return function () {
        plugins.del('public/assets/css/');
        plugins.del('public/assets/js/');
        plugins.del('public/build/');
    };
};