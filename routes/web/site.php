<?php

Route::group(['namespace' => 'Site'], function () {
    Route::get('/', [
        'as' => 'site.home',
        'uses' => 'HomeController@index'
    ]);
});